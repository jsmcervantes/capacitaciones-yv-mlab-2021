# Menú
- [Que es Gitlab y como acceder](#Que-es-Gitlab)
- [Crear repositorios](#Crear-repositorios)
- [Crear grupos](#Crear-grupos)
- [Crear subgrupos](#Crear-subgrupos)
- [Crear issues](#Crear-issues)
- [Crear labels](#Crear-labels)
- [Roles que cumplen](#Roles-que-cumplen)
- [Agregar miembros](#Agregar-miembros)
- [Crear boards y manejo de boards](#Crear-boards)

# Que es Gitlab
Es un servicio web de control de versiones y desarrollo de software colaborativo basado en Git. Además de gestor de repositorios, todo ello publicado bajo una Licencia de código abierto. 

Este software de control de versiones no centralizado, orientado al mantenimiento de versiones de aplicaciones con ficheros de código actualmente es el sistema de control de versiones más utilizado.

## Como acceder
- Abrir un navegador 
- Entrar a este enlace https://gitlab.com/
- E iniciar sesión **si no tiene cuenta Gtilab cree una**

# Crear Repositorios
     Para crear un nuevo repositorio, todo lo que necesita hacer es crear un nuevo proyecto o bifurcar un proyecto existente .
    Una vez que cree un nuevo proyecto, puede agregar nuevos archivos a través de la interfaz de usuario  o mediante la línea de comandos. Para agregar archivos desde la línea de comandos, siga las instrucciones que se presentan en la pantalla cuando cree un nuevo proyecto, o léalas en la documentación básica de la línea de comandos . 

# Crear Grupos
- Inicie sesion en su cuenta de GitLab y haga clic en el menu Grupos 

- Haga clic en el Boton **Nuevo grupo** para crear un grupo 

- Ingresa el nombre del grupo y si es va a ser **Privado o Público**

- Hemos creado un  **Grupo**

# Crear Subgrupos
    Para crear un subgrupo, debe ser propietario o mantenedor del grupo, según la configuración del grupo.
    
- En el panel del grupo, haga clic en el botón **Nuevo subgrupo** .

- Cree un nuevo grupo como lo harías normalmente. Observe que el espacio de nombres del grupo principal inmediato se fija en Ruta del grupo

- Haga clic en el botón **Crear grupo** para ser redirigido a la página del panel del nuevo grupo.


# Crear Issues
    Los problemas son el mecanismo fundamental en GitLab para colaborar en ideas, resolver problemas y planificar el trabajo.
- Discutir la implementación de una nueva idea.
- Seguimiento de tareas y estado laboral.
- Aceptar propuestas de funciones, preguntas, solicitudes de soporte o informes de errores.
- Elaborar nuevas implementaciones de código.

# Crear Labels
 El procedimiento es:
- Acceder al repositorio.
- Acceder a Repositorio -> Tags o Etiquetas. 
- Crear una nueva Etiqueta con los datos completos que nos interesen.

# Roles que cumplen
### Mantenedor
Acepta solicitudes de fusión en varios proyectos de GitLab
### Crítico	
Realiza revisiones de código en MR







