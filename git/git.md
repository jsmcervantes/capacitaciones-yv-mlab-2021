# Menú
- [Que es git](#¿Que-es-git?)  
- [Comandos de git en consola](#Comandos-de-git-en-consola)
- [Clientes git](#Clientes-git)
- [Clonación de proyecto por consola y por cliente](#Clonación-de-proyecto-por-consola-y-por-cliente)
- [Commits por consola y por cliente kraken o smart](#Commits-por-consola-y-por-cliente-kraken-o-smart)
- [Ramas de kraken](#Ramas-de-kraken)
- [Merge](#Merge)

# ¿Que es Git?

Git es una herramienta que realiza una función del control de versiones de código de forma distribuida, su propósito es llevar registro de los cambios en archivos de computadora incluyendo coordinar el trabajo que varias personas realizan sobre archivos compartidos en un repositorio de código.

# Comandos de Git en consola

### git init 

     Crea un repositorio de Git vacío o reinicia uno existente.
<img src="https://gitlab.com/jsmcervantes/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/init.PNG">

### git add .

    Agrega el contenido del archivo al índice
<img src="https://gitlab.com/jsmcervantes/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/add.jpg">

### git commit -m 

    Registra los cambios en el repositorio
<img src="https://gitlab.com/jsmcervantes/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/commit.PNG">

### git status

    Muestra el estado del árbol de trabajo
<img src="https://gitlab.com/jsmcervantes/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/status.PNG">

### git clone

    Clona un repositorio en un nuevo directorio
<img src="https://gitlab.com/jsmcervantes/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/clone.PNG">

### git log

    Muestra los registros de confirmación
<img src="https://gitlab.com/jsmcervantes/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/log.PNG">

### git push

    Se usa para cargar contenido del repositorio local a un repositorio remoto
<img src="https://gitlab.com/jsmcervantes/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/push.PNG">

### git pull

    Se emplea para extraer y descargar contenido desde un repositorio remoto y actualizar al instante el repositorio local para reflejar ese contenido
<img src="https://gitlab.com/jsmcervantes/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/pull.PNG">

### git touch 
    Crear una carpeta o archivo desde consola
<img src="https://gitlab.com/jsmcervantes/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/touch.PNG">

# Clientes Git

Un cliente GIT o software de control de versiones se usa mayormente para gestionar código fuente. Se diseñó para el mantenimiento de las versione de las aplicaciones cuando tienen un código fuente que contiene muchos archivos

<img src="https://gitlab.com/jsmcervantes/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/Cliente.PNG">

# Clonación de proyecto por consola y por cliente

## Clonación por consola
- En git escribir el comando **git clone**
<img src="https://gitlab.com/jsmcervantes/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/clone1.PNG">

- Y pulsar **enter**
<img src="https://gitlab.com/jsmcervantes/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/clone2.PNG">

## Clonación por cliente
- Entrar a **Kraken** y pulsar en la esquina superior izquierda **File** y escoger **Clone Repo** 

<img src="https://gitlab.com/jsmcervantes/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/file.PNG">

- Y seleccionar la carpeta y con que deseamos clonar nuestro repositorio 

<img src="https://gitlab.com/jsmcervantes/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/kraken.PNG">

# Commits por consola y por cliente kraken o smart

## Commits por Consola

En Gitbash escribir git add . 
<img src="../imagenes/git-add.png">

Escribir git commit -m “” y entre comillas el nombre
<img src="https://gitlab.com/jsmcervantes/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/commit-bash.png"> 

## Comits por cliente 

Desde Gitkraken seleccionar los archivos uno por uno y así crear los commits
<img src="https://gitlab.com/jsmcervantes/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/commit-kraken.png"> 

Una vez hechos los commits nos quedaría así 
<img src="https://gitlab.com/jsmcervantes/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/kraken-commit.png"> 

# Ramas de kraken

En nuestro cliente en mi caso **Git Kraken**  seleccionamos **branch** en la parte superior 
<img src="https://gitlab.com/jsmcervantes/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/rama1.png"> 

Pulsamos **Enter** y nuestra rama queda lista
<img src="https://gitlab.com/jsmcervantes/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/rama2.png"> 

# Merge

En **Kraken** arrastramos nuestra **rama** dentro de **master** y seleccionamos **merge rama1 in to master**
<img src="https://gitlab.com/jsmcervantes/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/merge1.png"> 

Con eso ya tendríamos hecho nuestro merge 
<img src="https://gitlab.com/jsmcervantes/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/merge2.png"> 